﻿using System;

namespace LatchObjectLib
{
    public class Latch : IDisposable
    {
        private readonly object _object;

        public Latch(object subject)
        {
            //add subject to monitored objects
            _object = subject;
            Extensions.LatchedObjects.Add(_object, this);
        }

        #region IDisposable Members

        public void Dispose()
        {
            //remove subject from monitored objects
            if(_object.IsLatched())
                Extensions.LatchedObjects.Remove(_object);
        }

        #endregion
    }
}