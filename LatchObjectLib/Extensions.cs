﻿using System;
using System.Collections.Generic;

namespace LatchObjectLib
{
    public static class Extensions
    {
        internal static IDictionary<object, Latch> LatchedObjects = new Dictionary<object, Latch>();

        public static IDisposable Latch<T>(this T subject)
        {
            return new Latch(subject);
        }

        public static void DoIfNotLatched<T>(this T subject, Action<T> action)
        {
            if (subject.IsLatched())
                return;
            
            action(subject);
        }

        public static TResult DoIfNotLatched<T, TResult>(this T subject, Func<T, TResult> action)
        {
            if (subject.IsLatched())
                return default(TResult);

            return action(subject);
        }

        public static void DoLatched<T>(this T subject, Action<T> action)
        {
            using (subject.Latch())
            {
                action(subject);
            }
        }

        public static TResult DoLatched<T, TResult>(this T subject, Func<T, TResult> action)
        {
            using (subject.Latch())
            {
                return action(subject);
            }
        }

        public static bool IsLatched<T>(this T subject)
        {
            Latch latch;
            return LatchedObjects.TryGetValue(subject, out latch);
        }

        public static EventHandlerWrapper<T> LatchHandler<T>(this T subject, Action<T, EventHandler> addHandler, Action<T, EventHandler> removeHandler)
        {
            return new EventHandlerWrapper<T>(subject, addHandler, removeHandler);
        }

        public static EventHandlerWrapper<T, TEventArgs> LatchHandler<T, TEventArgs>(this T subject, Action<T, EventHandler<TEventArgs>> addHandler, Action<T, EventHandler<TEventArgs>> removeHandler) where TEventArgs : EventArgs
        {
            return new EventHandlerWrapper<T, TEventArgs>(subject, addHandler, removeHandler);
        }

    }
}