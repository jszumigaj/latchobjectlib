﻿using System;

namespace LatchObjectLib
{
    public class EventHandlerWrapper<T> : IDisposable
    {
        private readonly T _subject;
        private readonly Action<T, EventHandler> _addHandler;
        private readonly Action<T, EventHandler> _removeHandler;
        private EventHandler _event;

        public EventHandlerWrapper(T subject, Action<T, EventHandler> addHandler, Action<T, EventHandler> removeHandler)
        {
            _subject = subject;
            _addHandler = addHandler;
            _removeHandler = removeHandler;
            _addHandler(_subject, Handler);
        }

        public event EventHandler Event
        {
            add { _event += value; }
            remove { _event -= value; }
        }

        private void Handler(object sender, EventArgs args)
        {
            if (_subject.IsLatched())
                return;
            if (_event != null)
                _event(sender, args);
        }

        public void Dispose()
        {
            _removeHandler(_subject, Handler);
        }
    }

    public class EventHandlerWrapper<T, TEventArgs> : IDisposable where TEventArgs : EventArgs
    {
        private readonly T _subject;
        private readonly Action<T, EventHandler<TEventArgs>> _addHandler;
        private readonly Action<T,  EventHandler<TEventArgs>> _removeHandler;
        private EventHandler<TEventArgs> _event;

        public EventHandlerWrapper(T subject, Action<T, EventHandler<TEventArgs>> addHandler, Action<T, EventHandler<TEventArgs>> removeHandler)
        {
            _subject = subject;
            _addHandler = addHandler;
            _removeHandler = removeHandler;
            _addHandler(_subject, Handler);
        }

        public event EventHandler<TEventArgs> Event
        {
            add { _event += value; }
            remove { _event -= value; }
        }


        private void Handler(object sender, TEventArgs eventArgs)
        {
            if (_subject.IsLatched())
                return;
            if (_event != null)
                _event(sender, eventArgs);
        }

        public void Dispose()
        {
            _removeHandler(_subject, Handler);
        }
    }   
}