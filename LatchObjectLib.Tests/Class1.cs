﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Windows.Forms;

namespace LatchObjectLib.Tests
{
    [TestFixture]
    public class Tests
    {
        TextBox textbox;
        int counter = 0;

        [SetUp]
        public void Setup()
        {
            counter = 0;
            textbox = new TextBox();
            textbox.TextChanged += new EventHandler(textbox_TextChanged);
        }

        void textbox_TextChanged(object sender, EventArgs e)
        {
            textbox.DoIfNotLatched(tb =>
            {
                counter++;
            });
        }


        [Test]
        public void Latch_can_be_use_with_using_statement()
        {
            textbox.Text = "first change";

            using (textbox.Latch())
            {
                textbox.Text = "New value";
            }

            textbox.Text = "last change";

            Assert.That(counter.Equals(2), "Expected counter equals 2 but was {0}", counter);
        }

        [Test]
        public void Latch_can_be_use_with_action()
        {
            textbox.Text = "first change";

            textbox.DoLatched(tb => tb.Text = "New value");

            textbox.Text = "last change";

            Assert.That(counter.Equals(2), "Expected counter equals 2 but was {0}", counter);
        }

        [Test]
        public void Latch_state_can_be_checked_via_IsLatched_method()
        {
            using (textbox.Latch())
            {
                Assert.That(textbox.IsLatched(), Is.True, "{0} should be latched, but isn't", textbox);
            }

            Assert.That(textbox.IsLatched(), Is.False, "{0} should be not latched, but is", textbox);
        }

        [Test]
        public void DoLatched_can_even_returns_value()
        {
            string value;

            textbox.Text = "first change";

            value = textbox.DoLatched(tb =>
            {
                textbox.Text = "test1";
                return textbox.Text;
            });

            Assert.That(value, Is.EqualTo("test1"));
            Assert.That(counter.Equals(1), "Expected counter equals 1 but was {0}", counter);
        }

        [Test]
        public void Event_can_be_raised_via_EventHandlerWrapper()
        {
            textbox = new TextBox();

            textbox.LatchHandler((t, h) => t.TextChanged += h, (t, h) => t.TextChanged -= h)
                   .Event += (o, args) => counter++;

            textbox.Text = "first change";

            using (textbox.Latch())
            {
                textbox.Text = "New value";
            }

            textbox.Text = "last change";

            Assert.That(counter.Equals(2), "Expected counter equals 2 but was {0}", counter);

        }
        
       

    }
}
